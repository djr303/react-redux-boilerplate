const client = require('scp2');
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const SQLITE_FILE_LOCATION = '/opt/dionaea/var/dionaea/dionaea.sqlite';
const HONEYPOT_IP = '45.76.128.176';

let username;
let password;

console.log('Attempting to download data...');

rl.question('Enter honeypot username: ', (answer) => {
  username = answer;
  rl.question('Enter honeypot password: ', (answer) => {
    password = answer;
    console.log('Downloading... (This may take a while, please wait)')
    client.scp(`${username}:${password}@${HONEYPOT_IP}:${SQLITE_FILE_LOCATION}`, './server/data/', function(err) {
        if (err) {
          console.log('Whoops! Something went wrong');
          console.log(err)
        } else {
          console.log('Data downloaded!');
        }
        process.exit();
    });
  });
});
