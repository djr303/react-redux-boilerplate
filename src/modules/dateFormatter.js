const dateFormatter = {
  formatUTC: (time_stamp) => {
    let date = parseFloat(time_stamp);
    let dateObj = new Date(0);
    date = date * 1000;
    dateObj.setUTCMilliseconds(date);
    return dateObj.toString();
  }
}

export default dateFormatter;
