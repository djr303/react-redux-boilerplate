import React from 'react'
import { IndexLink, Link } from 'react-router'
import { connect } from 'react-redux';
import PropTypes from 'prop-types'
import './PageLayout.scss'

export const PageLayout = ({ children, location }) => (
  <div className="conatiner">
    <h1>React / Redux Boilerplate</h1>
      {children}
  </div>
)

PageLayout.propTypes = {
  children: PropTypes.node,
}

const mapStateToProps = ({ location }) => {
  return { location }
}

export default connect(mapStateToProps)(PageLayout)
