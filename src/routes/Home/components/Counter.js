import React from 'react';
import PropTypes from 'prop-types';
import './Counter.scss';
import { connect } from 'react-redux';
import { incrementCounter } from '../../../store/counter'

class Counter extends React.Component {

  componentWillMount(){
    //mounting logic
  }

  _handleClick(e){
    e.preventDefault();
    this.props.dispatch(incrementCounter())
  }

  render(){
    return (
      <div>
        The current count is: {this.props.counter}
        <div>
          <button onClick={this._handleClick.bind(this)}>Increment</button>
        </div>
      </div>
    )
  }
}

Counter.propTypes = {
  counter: PropTypes.number.isRequired
};

Counter.defaultProps = {
  counter: 0
};

const mapStateToProps = ({ counter }) => {
  return { counter }
};

const mapDispatchToProps = (dispatch) => {
  return { dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
