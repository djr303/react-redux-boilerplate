import React from 'react'
import Counter from './Counter'

export const HomeView = () => (
  <div className="home">
    <h2>Increment this counter</h2>
    <Counter />
  </div>
)

export default HomeView
