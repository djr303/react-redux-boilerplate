//constants
export const INCREMENT_COUNTER = 'INCREMENT_COUNTER';

//actions
export function incrementCounter () {
  return {
    type: INCREMENT_COUNTER,
    payload : null
  }
}

//thunk actions
/* export function exampleThunkAction () {
  return function (dispatch) {
    return fetch('/api/...')
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        return dispatch(action(payload)...);
      });
  }
} */

//reducers
export default function counter (state = 0, action) {
  switch (action.type) {
    case INCREMENT_COUNTER:
      return state + 1;
    default:
      return state;
  }
}
